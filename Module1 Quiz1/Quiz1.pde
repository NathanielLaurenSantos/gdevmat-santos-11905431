float time = 0;
int globalVariable= 100;

void setup(){
  size(1280,720,P3D);
  camera(0,0, -(height / 2.0) / tan(PI * 30 / 180.0), 0, 0, 0, 0, -1, 0);
}

void draw(){
  background(0);
  drawPlane();
  drawLinear();
  drawQuadratic();
  drawSineWave();

}

void drawPlane(){

  color white = color(255, 255, 255);
  fill (white);
  stroke (white);
  
  line(-500, 0, 500, 0);
  line(0, -500, 0, 500);
  
  for(int i = -500; i <= 500; i += 10){
    
    line(i, -5, i, 5);
    line(-5, i, 5, i);
  
  }

}

void drawLinear(){
  strokeWeight(1);
  color purple = color(128,0,128);
  fill(purple);
  stroke(purple);
  
  for(int x = -200; x <= 200 ; x++){
  
    circle(x, ((-5 * x) + 30), 7);
  }
}

void drawQuadratic(){

  color yellow = color(255, 255, 0);
  fill(yellow);
  stroke(yellow);
  
  for(float x = -200; x <= 200; x += 0.1f){
    
    circle(x*2, ((x * x) - (15 * x) - 3), 5);
    
  }
  
}

void drawSineWave(){
  color red = color(255,0, 0);
  fill (red);
  stroke (red);
  
  for (float x= -200; x <= 200; x+=0.1f){
    time += 0.01f;
  circle(x *5,((float)Math.sin(x + time)*20),2);
  
  }


}
