void setup(){
  size(1080,720,P3D);
  camera(0, 0, Window.eyeZ, 0, 0, 0, 0, -1, 0);
}

  vecWalker newWalker = new vecWalker();

void draw(){
  
  
  
  //Uncomment Each to see that they still do work after replacing their Variables to Vector
  
  //newWalker.perlinWalk();
  //newWalker.randomWalk();
  //newWalker.randomWalkBiased();
  newWalker.moveAndBounce();
  newWalker.render();


}
