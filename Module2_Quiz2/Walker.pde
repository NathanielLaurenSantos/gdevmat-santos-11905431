class Walker{
  float x;
  float y;
  
  void render(){
  int rRng = int(random(1, 256));
  int gRng = int(random(1, 256));
  int bRng = int(random(1, 256));
  int aRng = int(random(50, 101));
  fill(rRng,gRng,bRng, aRng);
  
  circle(x, y, 50);
  }
  
  void randomWalk(){
    int rng = int(random(7));
    
    if (rng == 0){ 
    y+=10;
    }
    else if (rng == 1){
    y-= 10;
    }
    else if (rng == 2){
    x-= 10;
    }
    else if (rng == 3){
    x+= 10;
    }
    else if (rng == 4){
    x+= 10;
    y+= 10;
    }
    else if (rng == 5){
    x-= 10;
    y+= 10;
    }
    else if (rng == 6){
    x-= 10;
    y-= 10;
    }
    else if (rng == 7){
    x+= 10;
    y-= 10;
    }
  }
  
  void randomWalkBiased(){
  int rng = int(random(1, 101));
    if (rng <= 55){ 
    y-=10;
    }
    else if (rng <= 70 && rng >=56){
    y+= 10;
    }
    else if (rng <= 85 && rng >=71){
    x-= 10;
    }
    else if (rng <= 100 && rng >=86){
    x+= 10;
    }
  }

}
